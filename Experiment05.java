package experiments;
import java.util.*;
/******************************************************************************
 * This version moves from imperative style to declarative style programming.
 * The explicit loop is removed and a lamba expression is used to implement 
 * what was previously the body of a loop. A lamba expression is function
 * that can be passed as a parameter and does not have a name. Here the lambda
 * implements the Consumer interface, which takes a single parameter and 
 * produces no return value. 
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

import java.util.Arrays;

import football.Club;

public class Experiment05 {
	public static void main(String[] args) {
		List<Club> table = Arrays.asList(
				new Club(1, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			     new Club(2, "German", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(3, "LiverPool", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(4, "Manchester", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(5, "France", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(6, "Coratria", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(7, "Brazil", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(8, "Argentina", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(9, "Ireland", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(10, "Japan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(11, "China", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(12, "NewZealand", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ));
		 table.forEach(club -> System.out.println(club.getFootballClub()));
		
				
		
	}
	

}
