package experiments;
import java.util.*;
/******************************************************************************
 * This version is similar to the last but the interface implementation is
 * done more explicity. To do this the custom consumer has to implement the 
 * accept method.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/
import java.util.function.Consumer;

import football.Club;
public class Experiment06 {
			class ClubPrintConsumer implements Consumer<Club>{
			    public void accept(Club club) {
			      System.out.println(club.getFootballClub());     
			
		}
	}
			public void run() {
				List<Club> table = Arrays.asList(
						new Club(1, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
					     new Club(2, "German", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(3, "LiverPool", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(4, "Manchester", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(5, "France", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(6, "Coratria", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(7, "Brazil", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(8, "Argentina", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(9, "Ireland", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(10, "Japan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(11, "China", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
					     new Club(12, "NewZealand", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ));
				 table.forEach(club -> System.out.println(club.getFootballClub()));
				 
				 
				
			}
			 public static void main(String[] args) {
				    new Experiment06().run();
				  }
				}


