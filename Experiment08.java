package experiments;
import java.util.*;
import java.util.Arrays;
import java.util.List;
/******************************************************************************
 * This version uses a stream then a parallel stream to process the data. Note
 * that in the first version the names are printed in the order that they are 
 * stored in the list. In the second version the data is processed concurrently
 * so the order the names are printed cannot be determined. This demonstrates
 * that if we have a problem in which the order of the data is processed does
 * not matter then parallelism can be taken advantage of.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/
import football.Club;


public class Experiment08 {
	public void run() {
		List<Club> table = Arrays.asList(
				new Club(1, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			     new Club(2, "German", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(3, "LiverPool", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(4, "Manchester", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(5, "France", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(6, "Coratria", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(7, "Brazil", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(8, "Argentina", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(9, "Ireland", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(10, "Japan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(11, "China", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ),
			     new Club(12, "NewZealand", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ));
		
		System.out.println("Serial names\n---------");
	    table.stream().forEach(club -> System.out.println(club.getFootballClub()));

	    System.out.println("\nParallel names\n---------");
	    table.parallelStream().forEach(club -> System.out.println(club.getFootballClub()));
	}
		 
	    public static void main(String[] args) {
	        new Experiment08().run();
				

}
	}
